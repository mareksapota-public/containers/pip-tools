# Containers/pip-tools

Containerized [pip-tools][pip-tools].  This project provides a set of images
that can be used to run [pip-tools][pip-tools].  The images are based on
different versions of [Python][python-image] and you should match the image
version with the version of Python you intend to use.  Requirements compiled
with a different version of Python might lead to incorrect dependencies being
saved.  Only major Python versions are supported.  The base image is the latest
available [official Python image][python-image] for that major version.

## Available image tags

These are the currently available [image tags][container-registry]:

- `3.9`
- `3.10`
- `3.11`
- `3.12`
- `3.13`

The tags match the version of Python included.

## Example usage

You can use [podman][podman] to run the container like this:

```
podman run --rm -v './:/mnt/src/:Z' -w '/mnt/src/' \
    registry.gitlab.com/mareksapota-public/containers/pip-tools:3.13 \
    pip-compile requirements.in
```

Explanation of used flags:
- `--rm` - delete the container after the command is done.
- `-v './:/mnt/src/:Z'` - mount the current directory as `/mnt/src/` inside of
the container.
    - `:Z` - makes the volume private and only available to this container.
- `-w '/mnt/src/'` - set the working directory to `/mnt/src/` inside of the
container.
- `registry.gitlab.com/mareksapota-public/containers/pip-tools:3.11` - image to
use, in this case the Python 3.11 image is used.
- `pip-compile requirements.in` - command to execute.  Can include additional
parameters to `pip-compile`.

While the example uses [podman][podman], other OCI compatible runtimes can be
used to execute the image.

A more full example, including command output:

```console
$ echo -e 'django\nrequests' > requirements.in
$ podman run --rm -v './:/mnt/src/:Z' -w '/mnt/src/' \
    registry.gitlab.com/mareksapota-public/containers/pip-tools:3.11 \
    pip-compile requirements.in --resolver=backtracking
#
# This file is autogenerated by pip-compile with Python 3.11
# by the following command:
#
#    pip-compile --resolver=backtracking requirements.in
#
asgiref==3.6.0
    # via django
certifi==2022.12.7
    # via requests
charset-normalizer==3.0.1
    # via requests
django==4.1.5
    # via -r requirements.in
idna==3.4
    # via requests
requests==2.28.2
    # via -r requirements.in
sqlparse==0.4.3
    # via django
urllib3==1.26.14
    # via requests
```

## Updates

This image auto updates on a [weekly schedule][ci-schedule] based on the latest
available [Python images][python-image].

# Notice

These images are unofficial and not associated with neither the
[pip-tools][pip-tools] project nor the [Python][python] project.

As with many OCI images, these images contain software under various licenses
(such as any packages from the base distribution and/or indirect dependencies of
the primary software being contained).  As for any pre-built image usage, it is
the image user's responsibility to ensure that any use of these images complies
with any relevant licenses for all software contained within.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[pip-tools]: https://pypi.org/project/pip-tools/
[python-image]: https://hub.docker.com/_/python
[container-registry]: https://gitlab.com/mareksapota-public/containers/pip-tools/container_registry/3799922
[ci-schedule]: https://gitlab.com/mareksapota-public/containers/pip-tools/-/pipeline_schedules
[podman]: https://podman.io/
[python]: https://www.python.org/
