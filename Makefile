# MIT Expat License

# Copyright 2022-2025 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

PYTHON_VERSIONS := 3.9 3.10 3.11 3.12 3.13

.SECONDARY:

Containerfile.python%: Containerfile.template
	sed -e 's/PYTHON_VERSION/$(*F)/' Containerfile.template > 'Containerfile.python$(*F)'

build-% : Containerfile.python%
	buildah build -t 'pip-tools:$(*F)' -f 'Containerfile.python$(*F)'

run-% :
	podman run --rm -it 'localhost/pip-tools:$(*F)' bash

rmi-% :
	podman rmi 'localhost/pip-tools:$(*F)'

CONTAINER_FILES := $(addprefix Containerfile.python, $(PYTHON_VERSIONS))

.gitlab-ci.yml: $(CONTAINER_FILES) .gitlab-ci.build-script.template.yml .gitlab-ci.template.yml
	cp .gitlab-ci.template.yml .gitlab-ci.yml
	for PYTHON_VERSION in $(PYTHON_VERSIONS); do \
		sed -e "s/PYTHON_VERSION/$${PYTHON_VERSION}/" \
			-e 's/^\#.*$$//' \
			-e '/^$$/d' \
			.gitlab-ci.build-script.template.yml >> .gitlab-ci.yml ; \
	done

.PHONY: update-versions build rmi
update-versions: $(CONTAINER_FILES) .gitlab-ci.yml

build: $(addprefix build-, $(PYTHON_VERSIONS))

rmi: $(addprefix rmi-, $(PYTHON_VERSIONS))
